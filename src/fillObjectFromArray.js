import _ from 'lodash';

export default (array, value) =>
  array.reduce((map, item) => Object.assign(map, { [item]: _.isFunction(value) ? value(item) : _.cloneDeep(value) }), {});
