import _ from 'lodash';

const isNilOrEmptyString = value => value === '' || _.isNil(value);

export const preFilterSubs = (filters, subs) => {
    const filterEntries = Object.entries(filters);
    return subs.filter((sub) => {
        const length = sub.data.reduce((acc, item) => acc + item[5], 0);
        return filterEntries.every(([key, value]) => {
            if (key !== 'common') {
                if (value.percent === 0) {
                    return true
                }
                return value.percent <= ((value.items.reduce((acc, value) => acc + (sub.filters[key][value] || 0), 0) * 100) / length)
            }
            return true;
        })
    })
}


export const postFilterSubs = (filters, subs) => {
    return subs.filter((sub) => {
        if (!sub) {
            return false;
        }
        return _.entries(filters.common).every(([subKey, condition]) => {
            if (!condition) {
                return true;
            }
            if (_.isArray(condition)) {
                return condition.some(item => sub[subKey] === item)
            }
            if (_.isPlainObject(condition)) {
                if (_.has(condition, 'min') || _.has(condition, 'max')) {
                    return (isNilOrEmptyString(condition.min) || sub[subKey] >= +condition.min) && (isNilOrEmptyString(condition.max) || sub[subKey] <= +condition.max);
                }
                return true;
            }
            return condition === sub[subKey];
        })
    })
}