const findAverage = (items, key) => {
    const result = items.reduce(
        ([result, counter], { [key]: value }) => (value ? [result + value, counter + 1] : [result, counter]),
        [0, 0]
    );

    return result[0] / result[1] || 0;
};

export default subs => {
    return {
        averageROI: findAverage(subs, 'averageROI'),
        averageTrendFall: findAverage(subs, 'maxTrendFall')
    }
}