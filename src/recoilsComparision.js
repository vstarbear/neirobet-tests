import _ from 'lodash';

const calculateAverageAndRecoils = (data, period) => {
    let sum = 0;
    let values = [];
    let step = 0;

    const averages = data.reduce((result, item) => {
        step += 1;
        let dayProfit = item[4] - item[1]; // close - open
        sum += dayProfit;
        values.push(dayProfit);
        if (step > period) {
            sum -= values.shift();
            result.push(sum / period);
        } else {
            result.push(0)
        }
    }, []);

    const averageValue = _.sum(averages) / (data.length - period);

    const recoils = [];
    let isInRange = false;

    averages.forEach((value, index) => {
        if (averageValue * 1.3 <= value) {
            if (isInRange) {
                recoils[recoils.length - 1][1] = index;
            } else {
                recoils.push([index, index]);
                isInRange = true;
            }
        } else {
            isInRange = false;
        }
    });

    return {
        averages,
        recoils // Array of [startIndex, endIndex]
    }
};

export default async (data, periods) => {
    const averagesAndRecoils = periods.reduce((result, period) => Object.assign(result, {[period]: calculateAverageAndRecoils(data, period)}), {});
    const result = {};
    periods.forEach(period => {
        const current = averagesAndRecoils[period];
        result[period] = current.recoils.map(([startIndex, endIndex]) => {
            return periods.map(item => [
                current.averages[startIndex] - (averagesAndRecoils[item][startIndex] || 0),
                current.averages[endIndex] - (averagesAndRecoils[item][endIndex] || 0),

            ])
        });
    });
    return result;
} // объект матриц - каждая матрица - это сравнение всех средних и отскоков текущей средней с соответствующим периодом (ключ)
