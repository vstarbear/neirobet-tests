export default (value, precision = 2) =>
  Math.ceil(value * Math.pow(10, precision)) / Math.pow(10, precision);
